#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <vector>
#include"Polygon.h"
#include"Point.h"



class Triangle :public Polygon
{
public:
	Triangle(Point a, Point b, Point c);
	Point getP1() const;
	Point getP2() const;
	Point getP3() const;
	virtual std::vector<double> getSides() const;
	virtual double Triangle::perimeter() const;
	virtual double Triangle::area() const;
	virtual std::string getKind();
};

#endif 
