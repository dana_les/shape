#ifndef QUADTANGLE
#define QUADTANGLE

#include<iostream>
#include"Polygon.h"
#include <vector>



class Quadtngle: public Polygon
{
public:
	Quadtngle(Point a, Point b, Point c, Point d);
	Point getP1() const;
	Point getP2() const;
	Point getP3() const;
	Point getP4() const;
	virtual std::vector<double> getSides() const;
	virtual double perimeter() const;
	virtual double area() const;
	virtual std::string getKind();

};

#endif // !QUADTANGLE
