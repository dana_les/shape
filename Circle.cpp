
#include <iostream>
#include "shape.h"
#include "Point.h"
#include <vector>
#include "Circle.h"


Circle::Circle(Point p, double rad) : _center(p), _radius(rad){};

Point Circle::getCenter() const
{
	return(_center);
}

std::string Circle::getKind()
{
	return("Circle");
}

double Circle::getRadius() const
{
	return(_radius);
}

double Circle::perimeter() const
{
	return(3.14 * 2 * _radius);
}

double Circle::area() const
{
	return(3.14 * _radius * _radius);
}