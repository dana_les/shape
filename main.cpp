#include<fstream>
#include<istream>
#include<iostream>
#include<string>
#include"shape.h"
#include "Circle.h"
#include"Polygon.h"
#include"Quadrangle.h"
#include"Triangle.h"




int main()
{
	shape* shapes[5];
	int i;
	int kind;
	Point center(0,0), A(0,0),B(0,0), C(0,0), D(0,0);
	double X, Y;
	double radius;
	for (i = 0; i < 5; i++)
	{
		std::cout << "which shape do you want ? \n1  Circle\n2 - Quadtngle\n3 - Triangle\n";
		std::cin >> kind;
		if (kind == 1)
		{
			std::cout << "Enter center point (x,y) and radius:\n";
			std::cin >> X >> Y >> radius;
			center.move(X, Y);
			shapes[i] = new Circle(center, radius);
		}
		else if (kind == 2)
		{
			std::cout << "Enter first points (enter x, enter y): \n";
			std::cin >> X >> Y;
			A.move(X, Y);
			std::cout << "Enter secound points (enter x, enter y): \n";
			std::cin >> X >> Y;
			B.move(X, Y);
			std::cout << "Enter third points (enter x, enter y): \n";
			std::cin >> X >> Y;
			C.move(X, Y);
			std::cout << "Enter forth points (enter x, enter y): \n";
			std::cin >> X >> Y;
			D.move(X, Y);
			shapes[i] = new Quadtngle(A, B, C, D);
		}
		else if (kind == 3)
		{
			std::cout << "Enter first points (enter x, enter y): \n";
			std::cin >> X >> Y;
			A.move(X, Y);
			std::cout << "Enter secound points (enter x, enter y): \n";
			std::cin >> X >> Y;
			B.move(X, Y);
			std::cout << "Enter third points (enter x, enter y): \n";
			std::cin >> X >> Y;
			C.move(X, Y);
			shapes[i] = new Triangle(A, B, C);
		}
	}
	std::cout << "\nAreas: \n";
	for (i = 0 ; i < 5 ; i++)
	{
		std::cout << "Shape " << i << ": " << (std::string)shapes[i]->getKind() << ": " << shapes[i]->area() << " cm^2\n";
	}
	std::cout << "\mPerimeters: \n";
	for (i = 0; i < 5; i++)
	{
		std::cout << "Shape " << i << ": " << shapes[i]->getKind() << ": " << shapes[i]->perimeter() << " cm^2\n";
	}
	
	for (i = 0; i < 5; i++)
	{
		delete shapes[i];
	}
	system("pause");
	return(0);
}