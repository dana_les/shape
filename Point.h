#ifndef POINT_H
#define POINT_H


#include <iostream> 
#include <vector>



class Point
{
private:
	double _x;
	double _y;

public:
	Point(double x, double y);
	double getX() const;
	double getY() const;
	void move(double x, double y);
	void move(Point other);
	double distance(Point other);
	double get_H(Point a, Point b);
};

#endif // !POINT_H
