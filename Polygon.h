#ifndef POLYGON
#define POLYGON

#include <iostream>
#include <vector>
#include "shape.h"
#include "Point.h"



class Polygon: public shape
{
protected:
	std::vector<Point> _vertices;

public:
	Polygon(std::vector<Point> points);
	virtual int numOfPoints() const;
	virtual std::vector<double> getSides() const = 0;
	virtual std::vector<Point> getPoint() const;
	virtual double perimeter() const = 0;
	virtual double area() const = 0;
	virtual std::string getKind() = 0;;
};

#endif // !POLYGON
