
#include <iostream>
#include "Point.h"
#include <math.h>  
#include <vector>


Point::Point(double x, double y) : _x(x), _y(y){};

double Point::getX() const
{
	return(_x);
}

double Point::getY() const
{
	return(_y);
}

void Point::move(double x, double y)
{
	_x = x;
	_y = y;
}

void Point::move(Point other)
{
	_x = other.getX();
	_y = other.getY();
}

double Point::distance(Point other)
{
	double ret = (other._x - _x)*(other._x - _x) + (other._y - _y)*(other._y - _y);
	return (sqrt(ret));
}

double Point::get_H(Point a, Point b)
{
	double A,B,C;
	double abs_part, denominator;
	if ((a.getX() - b.getX()) == 0)
	{
		return(getX() -a.getX());
	}
	else
	{
		A = -((a.getY() - b.getY()) / (a.getX() - b.getX()));
		C = (-1)*A*a.getX() - a.getY();
		B = 1;
		abs_part = abs((A * getX()) + (B * getY()) + C);
		denominator = sqrt(A*A + B*B);
		return(abs_part / denominator);
	}


}
