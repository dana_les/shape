#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>
#include "shape.h"
#include "Point.h"
#include <vector>


class Circle: public shape
{
private:
	Point _center;
	double _radius;

public:
	Circle(Point p, double rad);
	Point getCenter() const;
	double getRadius() const;
	virtual double perimeter() const;
	virtual double area() const;
	virtual std::string getKind();
};

#endif // !CIRCLE_H
