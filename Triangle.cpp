#include <iostream>
#include "Polygon.h"
#include "Triangle.h"
#include "Point.h"
#include <vector>



Triangle::Triangle(Point a, Point b, Point c) :Polygon(std::vector<Point>{a, b, c}){};


Point Triangle::getP1() const
{
	return(_vertices[0]);
}
Point Triangle::getP2() const
{
	return(_vertices[1]);
}
Point Triangle::getP3() const
{
	return(_vertices[2]);
}

std::vector<double> Triangle::getSides() const
{
	std::vector<double> sides(3);
	Point A = _vertices[0];
	sides[0] = A.distance(_vertices[1]);
	A = _vertices[1];
	sides[1] = A.distance(_vertices[2]);
	A = _vertices[2];
	sides[2] = A.distance(_vertices[0]);
	return(sides);

}

double Triangle::perimeter() const
{
	std::vector<double> sides(3);
	sides = getSides();
	return(sides[0] + sides[1] + sides[2]);
}

double Triangle::area() const
{
	Point A = _vertices[2];
	double H = A.get_H(_vertices[0], _vertices[1]);
	A = _vertices[0];
	double dis = A.distance( _vertices[1]);
	return((H * dis) / 2);
}


std::string Triangle::getKind()
{
	return("Triangle");
}