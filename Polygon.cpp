#include <iostream>
#include "Polygon.h"
#include "Point.h"
#include <vector>



Polygon::Polygon(std::vector<Point> points) : _vertices(points){};

int Polygon::numOfPoints() const
{
	return (_vertices.size());
}



std::vector<Point> Polygon::getPoint() const
{
	return(_vertices);
}