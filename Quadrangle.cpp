#include "Quadrangle.h"
#include "Point.h"
#include "Triangle.h"
#include <vector>


Quadtngle::Quadtngle(Point a, Point b, Point c, Point d) :Polygon(std::vector<Point>{a, b, c ,d}){};

Point Quadtngle::getP1() const
{
	return(_vertices[0]);
}

Point Quadtngle::getP2() const
{
	return(_vertices[0]);
}

Point Quadtngle::getP3() const
{
	return(_vertices[0]);
}

Point Quadtngle::getP4() const
{
	return(_vertices[0]);
}

std::vector<double> Quadtngle::getSides() const
{
	std::vector<double> sides(4);
	Point A = _vertices[0];
	sides[0] = A.distance(_vertices[1]);
	A = _vertices[1];
	sides[1] = A.distance(_vertices[2]);
	A = _vertices[2];
	sides[2] = A.distance(_vertices[3]);
	A = _vertices[3];
	sides[3] = A.distance(_vertices[0]);
	return(sides);
}

double Quadtngle::perimeter() const
{
	std::vector<double> sides(4);
	sides = getSides();
	return(sides[0] + sides[1] + sides[2] + sides[3]);
}
double Quadtngle::area() const
{
	Triangle A(_vertices[0], _vertices[1], _vertices[2]);
	Triangle B(_vertices[0], _vertices[3], _vertices[2]);

	return(A.area() + B.area());
}

std::string Quadtngle::getKind()
{
	return("Quadtngle");
}
