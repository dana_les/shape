#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>



class shape
{
	public:
		virtual double perimeter() const = 0;
		virtual double area() const = 0;
		virtual std::string getKind() = 0;
};
#endif